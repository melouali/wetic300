<?php
	
	if ( is_admin() ) require_once( ABSPATH . 'wp-includes/pluggable.php');

	global $wpdb;

	$log = $wpdb->get_results( "

		SELECT *
		FROM    {$wpdb->prefix}pattern_log
		WHERE   time > date_sub(now(), interval 30 day)
		ORDER 	BY time DESC

	" );

	ob_start(); ?>

		<table style="width: 100%; text-align: left;">
			<thead>
				<tr>
					<th><?php _e( 'Login Time', 'plock-locale' ) ?></th>
					<th><?php _e( 'User', 'plock-locale' ) ?></th>
					<th><?php _e( 'From IP Address', 'plock-locale' ) ?></th>
					<th><?php _e( 'Status', 'plock-locale' ) ?></th>
				</tr>
			</thead>
			<tbody>

				<?php foreach( $log as $entry ) :
		$data = maybe_unserialize( $entry->data );
	$user = isset( $data[3] ) ? get_userdata( $data[3] ) : false; ?>
					<tr>
						<td><?php echo $entry->time; ?></td>
						<td><?php echo $user !== false ? $user->user_login : __( 'Unknown', 'plock-locale' ) ?></td>
						<td><?php echo isset( $data[0] ) ? $data[0] : '' ?></td>
						<td><?php echo $entry->status === '1' ? __( 'Successful', 'plock-locale' ) : __( 'Denied', 'plock-locale' ) ?></td>
					</tr>
				<?php endforeach; ?>

			</tbody>
		</table>

	<?php 
	
	$log = ob_get_clean();


/*	Start Admin Options 
	================================================= */
	$options = array();


/*	Customization
	================================================= */	
	$options[] = array( "name" => __( 'Customization','plock-locale' ),
						"type" => "section" );
						
	$options[] = array( "name" => __("Login URL",'plock-locale'),
			"desc" => __("This will be your patternlock login page", 'plock-locale'),
			"id" => PATTERNLOCK . "_url",
			"class" => "medium first",
			"placeholder" => "Placeholder",
			"std" => 'pattern-lock',
			"type" => "text");
	
	$options[] = array( "name" => __("Disable Default WP Login",'plock-locale'),
				"desc" => __("Activate this to use patternlock as the only login method", 'plock-locale'),
				"id" => PATTERNLOCK . "_wp_login",
				"class" => "medium last",
				"std" => 'false',
				"type" => "switch");					
						
	$options[] = array( "name" => __( "Login Box Position", 'plock-locale' ),
				"id" => PATTERNLOCK . "_login_position",
				"class" => "medium first",
				"std" => 'middle-center',
				"type" => "select",
				"options" => array(
					'top-left' => "Top Left",
					'top-center' => "Top Center",
					'top-right' => "Top Right",
					'middle-left' => "Middle Left",
					'middle-center' => "Middle Center",
					'middle-right' => "Middle Right",
					'bottom-left' => "Bottom Left",
					'bottom-center' => "Bottom Center",
					'bottom-right' => "Bottom Right",
				));					
	
	$options[] = array( "name" => __( "Logo", 'plock-locale' ),
				"id" => PATTERNLOCK . "_logo",
				"class" => "medium first",
				"std" => null,
				"type" => "upload_min" );
	
	$options[] = array( "name" => __( "Retina Logo", 'plock-locale' ),
				"id" => PATTERNLOCK . "_logo_retina",
				"class" => "medium last",
				"std" => null,
				"type" => "upload_min" );			
					
	$options[] = array( "name" => __( "Background Image", 'plock-locale' ),
				"id" => PATTERNLOCK . "_bg",
				"class" => "medium first",
				"std" => null,
				"type" => "upload_min" );
	
	$options[] = array( "name" => __( "Overlay Color",'plock-locale' ),
				"id" => PATTERNLOCK . "_color_overlay",
				"class" => "tiny",
				"std" => '#000000',
				"type" => "color");
	
	$options[] = array( "name" => __("Opacity",'plock-locale'),
				"id" => PATTERNLOCK  ."_overlay_opacity",
				"class" => "tiny",
				"std" => 75,
				"min" => 0,
				"max" => 100,
				"suffix" => "%",
				"increment" => 1,
				"type" => "number");					
	
	$options[] = array( "name" => __('Pattern Settings','plock-locale'),
						"type" => "section");
	
	$options[] = array( "name" => __("Preset Matrix Levels","CURLYTHEMES"),
				"desc" => __( "Please consider security reasons, a bigger grid means a safer login", 'plock-locale' ),
				"id" => PATTERNLOCK . "_preset_security",
				"std" => 0,
				"type" => "buttons",
				"options" => array(
					0 => __( "3 x 3", 'plock-locale' ),
					1 => __( "4 x 4", 'plock-locale' ),
					2 => __( "5 x 5", 'plock-locale' ),
					3 => __( "7 x 7", 'plock-locale' ),
					4 => __( '10 x 10', 'plock-locale' )
				));
	
	
	$options[] = array( "name" => __("Dots Columns",'plock-locale'),
				"id" => PATTERNLOCK  ."_matrix_columns",
				'desc' => __( 'Choose a custom number of columns for the grid', 'plock-locale' ),
				"class" => "medium first",
				"std" => 4,
				"min" => 3,
				"max" => 20,
				"increment" => 1,
				"type" => "number");
	
	$options[] = array( "name" => __("Dots Rows",'plock-locale'),
				"id" => PATTERNLOCK  ."_matrix_rows",
				'desc' => __( 'Choose a custom number of rows for the grid', 'plock-locale' ),
				"class" => "medium last",
				"std" => 4,
				"min" => 3,
				"max" => 20,
				"increment" => 1,
				"type" => "number");
	
	$options[] = array( "name" => __("Dots Margin",'plock-locale'),
				"id" => PATTERNLOCK  ."_matrix_margin",
				'desc' => __( 'Select the space between dots', 'plock-locale' ),
				"class" => "medium first",
				"std" => 20,
				"min" => 10,
				"max" => 100,
				"increment" => 1,
				"type" => "number");
	
	$options[] = array( "name" => __("Dots Radius",'plock-locale'),
				"id" => PATTERNLOCK  ."_matrix_radius",
				'desc' => __( 'Select the size of the dots', 'plock-locale' ),
				"class" => "medium last",
				"std" => 30,
				"min" => 10,
				"max" => 100,
				"increment" => 1,
				"type" => "number");
	
	$options[] = array( "name" => __( "Minimum Dots Required",'plock-locale' ),
				"id" => PATTERNLOCK  ."_matrix_required",
				"class" => "medium first",
				"std" => 5,
				"min" => 5,
				"max" => 10,
				"increment" => 1,
				"type" => "number");
	
	$options[] = array( "name" => __( "Security Level",'plock-locale' ),
				"id" => PATTERNLOCK  ."_matrix_level",
				"std" => "<div id='passwordStrength' data-s0='".__( 'Secured', 'plock-locale' )."' data-s1='".__( 'Restricted', 'plock-locale' )."' data-s2='".__( 'Secret', 'plock-locale' )."' data-s3='".__( 'Top Secret', 'plock-locale' )."' data-s4='".__( 'Thaumiel', 'plock-locale' )."'></div>",
				"class" => "medium last",
				"type" => "html");				
	
	$options[] = array( "name" => __('Login Attempts','plock-locale'),
						"type" => "section");
	
	$options[] = array( "name" => __("Enable PHP Sessions",'plock-locale'),
				"desc" => __("Add an extra layer of security by using PHP sessions. Please make sure your server allows the correct usage of session.", 'plock-locale'),
				"id" => PATTERNLOCK . '_force_session',
				"class" => "medium first",
				"std" => 'false',
				"type" => "switch");
	
	$options[] = array( "name" => __("Enable Cookies",'plock-locale'),
				"desc" => __("Add an extra layer of security by using cookies. Users without cookie support will not be able to login.", 'plock-locale'),
				"id" => PATTERNLOCK . '_force_scookie',
				"class" => "medium last",
				"std" => 'false',
				"type" => "switch");								
	
	$options[] = array( "name" => __('Stage 1 Lockdown','plock-locale'),
						"type" => "title",
						"desc" => __("Choose the behaviour for Stage 1 activation", 'plock-locale'));
	
	$options[] = array( "name" => __("Attempts Allowed",'plock-locale'),
				"id" => PATTERNLOCK  ."_att_1_allowed",
				"class" => "medium first",
				"std" => 3,
				"min" => 1,
				"max" => 10,
				"increment" => 1,
				"type" => "number");
	
	$options[] = array( "name" => __("Lockdown Time",'plock-locale'),
				"id" => PATTERNLOCK  ."_att_1_time",
				"class" => "medium last",
				"std" => 5,
				"min" => 1,
				"max" => 60,
				"increment" => 1,
				'suffix' => __(' mins', 'plock-locale'),
				"type" => "number");
				
	$options[] = array("type" => "divider");			
	
	$options[] = array( "name" => __('Stage 2 Lockdown','plock-locale'),
						"type" => "title",
						"desc" => __("Choose the behaviour for Stage 2 activation", 'plock-locale'));
	
	$options[] = array( "name" => __("Attempts Allowed",'plock-locale'),
				"id" => PATTERNLOCK  ."_att_2_allowed",
				"class" => "medium first",
				"std" => 2,
				"min" => 1,
				"max" => 5,
				"increment" => 1,
				"type" => "number");
	
	$options[] = array( "name" => __("Lockdown Time",'plock-locale'),
				"id" => PATTERNLOCK  ."_att_2_time",
				"class" => "medium last",
				"std" => 10,
				"min" => 1,
				"max" => 120,
				"increment" => 1,
				'suffix' => __(' mins', 'plock-locale'),
				"type" => "number");
	
	$options[] = array("type" => "divider");
	
	$options[] = array( "name" => __('Stage 3 Lockdown','plock-locale'),
						"type" => "title",
						"desc" => __("Choose the behaviour for Stage 3 activation", 'plock-locale'));
	
	$options[] = array( "name" => __("Attempts Allowed",'plock-locale'),
				"id" => PATTERNLOCK  ."_att_3_allowed",
				"class" => "medium first",
				"std" => 1,
				"min" => 1,
				"max" => 5,
				"increment" => 1,
				"type" => "number");
	
	$options[] = array( "name" => __("Lockdown Time",'plock-locale'),
				"id" => PATTERNLOCK  ."_att_3_time",
				"class" => "medium last",
				"std" => 2,
				"min" => 1,
				"max" => 365,
				"increment" => 1,
				'suffix' => __(' days', 'plock-locale'),
				"type" => "number");
	
	$options[] = array("type" => "divider");
	
	$options[] = array( "name" => __('Initiate Total Lockdown (Under Attack - Global Setting)','plock-locale'),
						"type" => "title",
						"desc" => __("Choose the behaviour for Total Lockdown activation", 'plock-locale'));
	
	$options[] = array( "name" => __("Max Attempts Allowed",'plock-locale'),
				"id" => PATTERNLOCK  ."_att_lockdown",
				"class" => "medium first",
				"std" => 20,
				"min" => 10,
				"max" => 120,
				"increment" => 5,
				"type" => "number");
	
	$options[] = array( "name" => __("Max Time Allowed",'plock-locale'),
				"id" => PATTERNLOCK  ."_att_lockdown_time",
				"class" => "medium last",
				"std" => 30,
				"min" => 10,
				"max" => 60,
				"increment" => 1,
				'suffix' => __(' mins', 'plock-locale'),
				"type" => "number");
	
	$link = site_url( '/' . md5( PATTERNLOCK_KEY ) . '/' );			
	
	$options[] = array( "name" => __("Lockdown Temporary URL",'plock-locale'),
				"desc" => __( "This URL is only available after the public login URL has need locked due to possible attacks. Please save this link for future reference or add it as Favourite in your browser, because, if you disable WP default login, it will be the only way you can access the website during Total Lockdown.", 'plock-locale' ),
				"id" => PATTERNLOCK  ."_temporary",
				'std' => "<a href='$link' target='_blank'>$link</a>",
				"type" => "html");	
	
	
	$options[] = array( "name" => __( "Mass E-mail Notification", 'plock-locale' ),
				"desc" => __( "Check this if you want to be notified via email, when your website is under attack. The public login<br> address will be locked and the temporary lockdown URL will be available.", 'plock-locale' ),
				"id" => PATTERNLOCK . "_lockdown_mail",
				"std" => 'true',
				"type" => "switch");															
	
	$options[] = array( "name" => __('Whitelist & Blacklist','plock-locale'),
						"type" => "section");
	
	$options[] = array( "name" => __( "Whitelist", 'plock-locale' ),
				"desc" => __( "Use commas to separate the IP addresses that you want to include in the whitelist", 'plock-locale' ),
				"id" => PATTERNLOCK . "_whitelist",
				"class" => "medium first",
				"placeholder" => "192.168.1.1, 192.168.14.23, 8.8.8.8, ...",
				"std" => null,
				"type" => "textarea");
	
	$options[] = array( "name" => __( "Blacklist", 'plock-locale' ),
				"desc" => __( "Use commas to separate the IP addresses that you want to include in the blacklist", 'plock-locale' ),
				"id" => PATTERNLOCK . "_blacklist",
				"class" => "medium last",
				"placeholder" => "192.168.1.1, 192.168.14.23, 8.8.8.8, ...",
				"std" => null,
				"type" => "textarea");
	
	$options[] = array( "name" => __('System Notifications','plock-locale'),
						"type" => "section");					
	
	$options[] = array( "name" => __("Stage 1 Attempts Notification",'plock-locale'),
			"desc" => __("Use the %d symbol to reference the number of attempts", 'plock-locale'),
			"id" => PATTERNLOCK . "_notif_stage_1",
			"class" => "medium first",
			"std" => 'Invalid Pattern. %d Attempts left!',
			"type" => "text");
	
	$options[] = array( "name" => __("Stage 1 Lockdown Notification",'plock-locale'),
			"desc" => __("Use the %d symbol to reference the remaining time", 'plock-locale'),
			"id" => PATTERNLOCK . "_notif_stage_1_lock",
			"class" => "medium last",
			"std" => 'Invalid Pattern. You have been locked out for %d minutes!',
			"type" => "text");
	
	$options[] = array( "name" => __("Stage 2 Attempts Notification",'plock-locale'),
			"desc" => __("Use the %d symbol to reference the number of attempts", 'plock-locale'),
			"id" => PATTERNLOCK . "_notif_stage_2",
			"class" => "medium first",
			"std" => 'Invalid Pattern. %d Attempts left!',
			"type" => "text");
	
	$options[] = array( "name" => __("Stage 2 Lockdown Notification",'plock-locale'),
			"desc" => __("Use the %d symbol to reference the remaining time", 'plock-locale'),
			"id" => PATTERNLOCK . "_notif_stage_2_lock",
			"class" => "medium last",
			"std" => 'Invalid Pattern. You have been locked out for %d minutes!',
			"type" => "text");
	
	$options[] = array( "name" => __("Stage 3 Attempts Notification",'plock-locale'),
			"desc" => __("Use the %d symbol to reference the number of attempts", 'plock-locale'),
			"id" => PATTERNLOCK . "_notif_stage_3",
			"class" => "medium first",
			"std" => 'Invalid Pattern. %d Attempts left!',
			"type" => "text");
	
	$options[] = array( "name" => __("Stage 3 Lockdown Notification",'plock-locale'),
			"desc" => __("Use the %d symbol to reference the remaining days", 'plock-locale'),
			"id" => PATTERNLOCK . "_notif_stage_3_lock",
			"class" => "medium last",
			"std" => 'Invalid Pattern. You have been locked out for %d days!',
			"type" => "text");
	
	$options[] = array( "name" => __('Lockdown Labels','plock-locale'),
						"type" => "section");
	
	$options[] = array( "name" => __("Lockdown Title",'plock-locale'),
			"desc" => __("Use this field to change the title of the page", 'plock-locale'),
			"id" => PATTERNLOCK . "_lock",
			"class" => "medium first",
			"std" => 'Lockdown',
			"type" => "text");
	
	$options[] = array( "name" => __("Second (singular form)",'plock-locale'),
			"id" => PATTERNLOCK . "_lock_sec",
			"class" => "medium first",
			"std" => 'second',
			"type" => "text");
	
	$options[] = array( "name" => __("Seconds (plural form)",'plock-locale'),
			"id" => PATTERNLOCK . "_lock_secs",
			"class" => "medium last",
			"std" => 'seconds',
			"type" => "text");
	
	$options[] = array( "name" => __("Minute (singular form)",'plock-locale'),
			"id" => PATTERNLOCK . "_lock_min",
			"class" => "medium first",
			"std" => 'minute',
			"type" => "text");
	
	$options[] = array( "name" => __("Minutes (plural form)",'plock-locale'),
			"id" => PATTERNLOCK . "_lock_mins",
			"class" => "medium last",
			"std" => 'minutes',
			"type" => "text");																					
	
	$options[] = array( "name" => __("Hour (singular form)",'plock-locale'),
			"id" => PATTERNLOCK . "_lock_hour",
			"class" => "medium first",
			"std" => 'hour',
			"type" => "text");
	
	$options[] = array( "name" => __("Hours (plural form)",'plock-locale'),
			"id" => PATTERNLOCK . "_lock_hours",
			"class" => "medium last",
			"std" => 'hours',
			"type" => "text");
	
	$options[] = array( "name" => __("Day (singular form)",'plock-locale'),
			"id" => PATTERNLOCK . "_lock_day",
			"class" => "medium first",
			"std" => 'day',
			"type" => "text");
	
	$options[] = array( "name" => __("Days (plural form)",'plock-locale'),
			"id" => PATTERNLOCK . "_lock_days",
			"class" => "medium last",
			"std" => 'days',
			"type" => "text");
	
	$options[] = array( "name" => __("Month (singular form)",'plock-locale'),
			"id" => PATTERNLOCK . "_lock_month",
			"class" => "medium first",
			"std" => 'month',
			"type" => "text");
	
	$options[] = array( "name" => __("Months (plural form)",'plock-locale'),
			"id" => PATTERNLOCK . "_lock_months",
			"class" => "medium last",
			"std" => 'months',
			"type" => "text");
	
	$options[] = array( "name" => __('Forgot Password','plock-locale'),
						"type" => "section");
	
	$options[] = array( "name" => __("Forgot Password Secret Token",'plock-locale'),
			"id" => PATTERNLOCK . "_forgot_token",
			"class" => "medium first",
			"desc" => __( 'Use this token in the input field after the username or email address separated with a space as such: \'admin token\' or \'email@email.com token\'', 'plock-locale' ),
			"type" => "text");
	
	$options[] = array( "name" => __("Threshold",'plock-locale'),
				"id" => PATTERNLOCK  ."_forgot_threshold",
				"class" => "medium last",
				"std" => 30,
				"min" => 5,
				"max" => 180,
				"suffix" => " minutes",
				"increment" => 5,
				'desc' => __( 'Each IP Address is allowed 5 valid requests for each threshold. By default each IP address is allowed to request 5 valid emails for every 30 minutes', 'plock-locale' ),
				"type" => "number");		
	
	$options[] = array( "name" => __("Form Title",'plock-locale'),
			"id" => PATTERNLOCK . "_forgot_title",
			"class" => "medium first",
			"std" => 'Reset Your Password',
			"type" => "text");	
	
	$options[] = array( "name" => __("Field Label",'plock-locale'),
			"id" => PATTERNLOCK . "_forgot_field",
			"class" => "medium last",
			"std" => 'Your Username or E-mail Address',
			"type" => "text");
		
	$options[] = array( "name" => __("Submit Button",'plock-locale'),
			"id" => PATTERNLOCK . "_forgot_submit",
			"class" => "medium first",
			"std" => 'Reset Password',
			"type" => "text");	
	
	$options[] = array( "name" => __("Cancel Button",'plock-locale'),
			"id" => PATTERNLOCK . "_forgot_cancel",
			"class" => "medium last",
			"std" => 'Cancel',
			"type" => "text");	
	
	$options[] = array( "name" => __("Reset Password Notification",'plock-locale'),
			"id" => PATTERNLOCK . "_forgot_notification",
			"class" => "medium first",
			"std" => 'If your e-mail or username is valid, you will receive a recovery email.',
			"type" => "textarea");	
	
	$options[] = array( "name" => __("Threshold Notification",'plock-locale'),
			"id" => PATTERNLOCK . "_forgot_notif_t",
			"class" => "medium last",
			"std" => 'Hey, Hey! Slow down buddy!',
			"type" => "textarea");										
	
	$options[] = array( "name" => __('Access Log (30 days)','plock-locale'),
						"type" => "section");				
	
	$options[] = array( "name" => __("Access Log for last 30 days",'plock-locale'),
				"id" => PATTERNLOCK  ."_log",
				'std' => "$log",
				"type" => "html");
	
	$options[] = array( "name" => __('Back-up & Reset','plock-locale'),
						"type" => "section");
	
	// Backup Field					
	$options[] = array( "name" => __("Backup Settings",'plock-locale' ),
				"desc" => __("Use this area to backup your Pattern Lock Settings", 'plock-locale' ),
				"id" => PATTERNLOCK."_textarea",
				"class" => "medium first",
				"type" => "backup");
	
	// Reset					
	$options[] = array( "name" => __("Pattern Lock Defaults",'plock-locale' ),
				"desc" => __("Restore all your Pattern Lock settings to their default values. Warning: All your existing settings will be lost. Please make sure you run a backup first.", 'plock-locale' ),
				"class" => "medium last",
				"type" => "reset");				

																																																					
?>
