<?php
/*
Plugin Name: Pattern Lock Login
Plugin URI: http://demo.curlythemes.com/wordpress-login-plugin/
Description: Secure and simple WordPress login with unique patterns for every user.
Version: 1.0
Author: Curly Themes
Author URI: http://www.curlythemes.com
Text Domain: plock-locale
*/

/** Define the Options Prefix */
define( 'PATTERNLOCK', 'pattern_lock' );
define( 'PATTERNLOCK_KEY', hash( 'sha256', DB_USER . DB_PASSWORD ) );

if( is_admin() ){

	if( ! class_exists( 'WhitelabelOptions' ) ){

		include( 'admin/admin-page.php' );

		include( 'admin/options.php' );

		$options_page = new WhitelabelOptions(
			'Pattern Lock',
			'pattern-lock',
			PATTERNLOCK,
			'options-general.php',
			null,
			'read',
			null,
			true,
			false,
			true,
			$options,
			WP_PLUGIN_URL . '/pattern-lock/'
		);
	}
}

register_activation_hook( __FILE__, array( 'PatternLock', 'install' ) );

include('class.pattern-lock.php');
include('class.user-profile.php');

?>