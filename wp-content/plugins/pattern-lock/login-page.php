<?php
	
	/**
	define('DONOTCACHEPAGE', true);
	//Disables page caching for a given page.
	define('DONOTCACHEDB', true);
	//Disables database caching for given page.
	define('DONOTMINIFY', true);
	//Disables minify for a given page.
	define('DONOTCDN', true);
	//Disables content delivery network for a given page.
	define('DONOTCACHCEOBJECT', true);
	//Disables object cache for a given page
	*/
	
	if ( ! defined( 'ABSPATH' ) ) {
		
		exit; 
		
	}
	
	if( ! self::user_access() ){
		
		exit;
		
	}
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">

  <title><?php echo get_bloginfo( 'title' ) ?> <?php _e( 'Login', 'plock-locale' ); ?></title>

<link rel="stylesheet" href="<?php echo WP_PLUGIN_URL . '/pattern-lock/lib/patternLock.css' ?>">
<link rel="stylesheet" href="<?php echo WP_PLUGIN_URL . '/pattern-lock/lib/alertify.css' ?>">
  
	<style media="all">
		
		<?php 
			
			$color_overlay = esc_attr( get_option( PATTERNLOCK . "_color_overlay", '#000000' ) );
			$color_opacity = intval( esc_attr( get_option( PATTERNLOCK . "_overlay_opacity", 75 ) ) ) / 100;
			
			$overlay = PatternLock::hex2rgb($color_overlay) ? 
				'rgba( ' . PatternLock::hex2rgb($color_overlay) . ',' . $color_opacity . ' )' : 
				'rgba( 0,0,0,' . $color_opacity . ' )';
				
			$overlay_box = PatternLock::hex2rgb($color_overlay) ? 
				'rgba( ' . PatternLock::hex2rgb($color_overlay) . ',' . ( 1.25 - $color_opacity  )  . ' )' : 
				'rgba( 0,0,0,' . ( 1.25 - $color_opacity ) . ' )';
				
		?>
		
		html, body, div, span, applet, object, iframe,
		h1, h2, h3, h4, h5, h6, p, blockquote, pre,
		a, abbr, acronym, address, big, cite, code,
		del, dfn, em, img, ins, kbd, q, s, samp,
		small, strike, strong, sub, sup, tt, var,
		b, u, i, center,
		dl, dt, dd, ol, ul, li,
		fieldset, form, label, legend,
		table, caption, tbody, tfoot, thead, tr, th, td,
		article, aside, canvas, details, embed, 
		figure, figcaption, footer, header, hgroup, 
		menu, nav, output, ruby, section, summary,
		time, mark, audio, video {
			margin: 0;
			padding: 0;
			border: 0;
			font-size: 100%;
			font: inherit;
			vertical-align: baseline;
		}
		/* HTML5 display-role reset for older browsers */
		article, aside, details, figcaption, figure, 
		footer, header, hgroup, menu, nav, section {
			display: block;
		}
		body {
			line-height: 1;
		}
		ol, ul {
			list-style: none;
		}
		blockquote, q {
			quotes: none;
		}
		blockquote:before, blockquote:after,
		q:before, q:after {
			content: '';
			content: none;
		}
		table {
			border-collapse: collapse;
			border-spacing: 0;
		}
			  
		html,
		body{
		  margin: 0;
		  padding: 0;
		  position: relative;
		  font-family: 
		}
		body{
			font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen", "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue", sans-serif;
			font-size: 14px;
		}
		
		<?php if( esc_attr( get_option( PATTERNLOCK . "_bg" ) ) ) : ?>
		body{
		  background-image: url('<?php echo esc_attr( get_option( PATTERNLOCK . "_bg" ) ) ?>');
		  background-size: cover;
		  background-attachment: fixed;
		  
		}
		<?php endif; ?>
		
		body::before{
			content: ' ';
			position: fixed;
			display: block;
			top: 0;
			right: 0;
			bottom: 0;
			left: 0;
			background: <?php echo $overlay ?>;
		}
		#PatternLock-container{
			margin: 0;
			padding: 0;
			text-align: center;
			position: fixed;
		}
		#PatternLock-container.top-left{
			top: 80px;
			left: 80px;
		}
		#PatternLock-container.top-center{
			left: 50%;
			top: 80px;
			-webkit-transform: translateX(-50%);
			-ms-transform: translateX(-50%);
			transform: translateX(-50%);
		}
		#PatternLock-container.top-right{
			top: 80px;
			right: 80px;
		}
		#PatternLock-container.middle-left{
			left: 80px;
			top: 50%;
			-webkit-transform: translateY(-50%);
			-ms-transform: translateY(-50%);
			transform: translateY(-50%);
		}
		#PatternLock-container.middle-center{
			left: 50%;
			top: 50%;
			-webkit-transform: translate(-50%, -50%);
			-ms-transform: translate(-50%, -50%);
			transform: translate(-50%, -50%);
		}
		#PatternLock-container.middle-right{
			right: 80px;
			top: 50%;
			-webkit-transform: translateY(-50%);
			-ms-transform: translateY(-50%);
			transform: translateY(-50%);
		}
		#PatternLock-container.bottom-left{
			bottom: 80px;
			left: 80px;
		}
		#PatternLock-container.bottom-center{
			left: 50%;
			bottom: 80px;
			-webkit-transform: translateX(-50%);
			-ms-transform: translateX(-50%);
			transform: translateX(-50%);
		}
		#PatternLock-container.bottom-right{
			bottom: 80px;
			right: 80px;
		}
		img + #PatternLock{
			margin-top: 20px;
		}
		#PatternLock{
			margin: 0;
			padding: 0;
			background: <?php echo $overlay_box ?>;
		}
		#PatternLock > ul{
			margin: 0;
			padding: 0;
		}
		#PatternLock .patt-dots{
			
			border: none;
			border-radius: 100%;
		}
		
		#escape{
			color: <?php echo PatternLock::brightness( $color_overlay, 0.65 ) ?>;
			font-size: 8px;
			letter-spacing: 2px;
			font-family: sans-serif;
			text-transform: uppercase;
			display: block;
			text-align: center;
			opacity: 0.55;
			margin-top: 20px;
			display: none;
		}
		#escape kbd{
			display: inline-block;
			padding: 4px 4px 4px 6px;
			border: 1px solid <?php echo PatternLock::brightness( $color_overlay, 0.75 ) ?>;
			color: <?php echo PatternLock::brightness( $color_overlay, 1 ) ?>;
			background: <?php echo PatternLock::brightness( $color_overlay, 0.15 ) ?>;
			border-radius: 1px;
			border-bottom-width: 2px;
			margin: 0 3px;
			font-family: inherit;
			line-height: inherit;
			font-weight: bold;
			text-align: center;
			text-decoration: none;
			-moz-border-radius: .3em;
			-webkit-border-radius: .3em;
			border-radius: .3em;
			cursor: default;
			-moz-user-select: none;
			-webkit-user-select: none;
			user-select: none;
			cursor: pointer;
			letter-spacing: 1px;
		}
		
		.patt-wrap {
			margin: auto;
			overflow: hidden
		}
		
		.patt-wrap li {
			transition: all .4s ease-in-out 0s
		}
		
		.patt-dots,
		.patt-lines {
			transition: background .1s ease-in-out 0s
		}
		
		.patt-circ {
			border: 3px solid transparent
		}
		
		.patt-dots {
			background: <?php echo PatternLock::brightness( $color_overlay, 0.8 ) ?>;
		}
		
		.patt-lines {
			background: <?php echo PatternLock::brightness( $color_overlay, 0.4 ) ?>;
			
			<?php if( get_option( PATTERNLOCK . '_privacy' ) === 'true' ) : ?>
			background: transparent !important;
			<?php endif; ?>
		}
		
		.patt-circ.hovered {
			border-color: <?php echo PatternLock::brightness( $color_overlay, 0.8 ) ?>;
			background: <?php echo PatternLock::brightness( $color_overlay, 0.2 ) ?>;
			
			<?php if( get_option( PATTERNLOCK . '_privacy' ) === 'true' ) : ?>
			background: transparent !important;
			border-color: transparent !important;
			<?php endif; ?>
		}
		
		.patt-error .patt-circ.hovered {
			background: rgba(243, 66, 53, .4);
			border-color: rgba(243, 66, 53, .8)
		}
		
		.patt-error .patt-lines {
			background: rgba(243, 66, 53, .5)
		}
		
		.patt-success .patt-circ.hovered {
			background: rgba(75, 174, 79, .4);
			border-color: rgba(75, 174, 79, .8)
		}
		
		.patt-success .patt-lines {
			background: rgba(75, 174, 79, .5)
		}
		.patt-circ:nth-child(1),
		.patt-circ:nth-child(2),
		.patt-circ:nth-child(3) {
			animation: fadeInUp .4s
		}
		
		.patt-circ:nth-child(4),
		.patt-circ:nth-child(5),
		.patt-circ:nth-child(6) {
			animation: fadeInUp .6s
		}
		
		.patt-circ:nth-child(7),
		.patt-circ:nth-child(8),
		.patt-circ:nth-child(9) {
			animation: fadeInUp .8s
		}
		.logo-retina{
			display: none;
		}
		@media (-webkit-min-device-pixel-ratio: 1.5), (min-resolution: 144dpi) {
			img.logo-nonretina {
				display: none; 
			}
			
			img.logo-retina {
				display: inline-block; 
			} 
    	}
    	
    	#counter{
	    	min-width: 400px;
			margin: 0;
			padding: 50px 100px;
			background: <?php echo $overlay_box ?>;
			display: block;
			color: <?php echo PatternLock::brightness( $color_overlay, 1 ) ?>;
			font-size: 18px;
			overflow: hidden;
			position: relative;
			text-transform: capitalize;
			display: none;
		}
		#counter h1{
			margin-top: 0;
			margin-bottom: 0;
		}
		.spinner {
		  margin: 100px auto;
		  width: 50px;
		  height: 40px;
		  text-align: center;
		  font-size: 10px;
		}
		
		.spinner > div {
		  background-color: <?php echo PatternLock::brightness( $color_overlay, 1 ) ?>;
		  height: 100%;
		  width: 6px;
		  display: inline-block;
		  
		  -webkit-animation: sk-stretchdelay 1.2s infinite ease-in-out;
		  animation: sk-stretchdelay 1.2s infinite ease-in-out;
		}
		
		.spinner .rect2 {
		  -webkit-animation-delay: -1.1s;
		  animation-delay: -1.1s;
		}
		
		.spinner .rect3 {
		  -webkit-animation-delay: -1.0s;
		  animation-delay: -1.0s;
		}
		
		.spinner .rect4 {
		  -webkit-animation-delay: -0.9s;
		  animation-delay: -0.9s;
		}
		
		.spinner .rect5 {
		  -webkit-animation-delay: -0.8s;
		  animation-delay: -0.8s;
		}
		
		@-webkit-keyframes sk-stretchdelay {
		  0%, 40%, 100% { -webkit-transform: scaleY(0.4) }  
		  20% { -webkit-transform: scaleY(1.0) }
		}
		
		@keyframes sk-stretchdelay {
		  0%, 40%, 100% { 
		    transform: scaleY(0.4);
		    -webkit-transform: scaleY(0.4);
		  }  20% { 
		    transform: scaleY(1.0);
		    -webkit-transform: scaleY(1.0);
		  }
		}
		
		h1{
			font-weight: bold;
			font-size: xx-large;
			line-height: 1.45;
			margin-bottom: 10px;
		}
		.logo{
			margin-bottom: 20px;
		}
		  
	</style>

  <!--[if lt IE 9]>
  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
</head>
<?php 
	
	$logo 		 = esc_url_raw( get_option( PATTERNLOCK . '_logo' ) );
	$logo_retina = esc_url_raw( get_option( PATTERNLOCK . '_logo_retina' ) );

	
?>
<body>
	
	<div id="PatternLock-container" class="<?php echo esc_attr( get_option( PATTERNLOCK . "_login_position", 'middle-center' ) ); ?>">
		
		<?php if( $logo ) : ?>
			<img src="<?php echo $logo ?>" class="logo logo-nonretina">
		<?php endif; ?>
		
		<?php if( $logo_retina ) : list( $width, $height, $type, $attr ) = getimagesize( $logo_retina );?>
			<img src="<?php echo $logo_retina ?>" class="logo logo-retina" width="<?php echo $width / 2 ?>" height="<?php echo $height / 2 ?>">
		<?php endif; ?>
		
		<div class="spinner">
			<div class="rect1"></div>
			<div class="rect2"></div>
			<div class="rect3"></div>
			<div class="rect4"></div>
			<div class="rect5"></div>
		</div>
		
		<div id="counter"></div>
		
		<div id="PatternLock"></div>
		
		<div id="escape"><?php printf( __( 'Press %s to recover password' , 'plock-locale' ), '<kbd>ESC</kbd>' ) ?></div>
		
	</div>
	<script type="text/javascript"> 
		/* <![CDATA[ */
		var PatternLockData = {
			"matrix"	: {
				"cols"	: "<?php echo esc_attr( get_option( PATTERNLOCK  . "_matrix_columns", 3 ) ) ?>",
				"rows"	: "<?php echo esc_attr( get_option( PATTERNLOCK  . "_matrix_rows", 3 ) ) ?>",
				"margin": "<?php echo esc_attr( get_option( PATTERNLOCK  . "_matrix_margin", 20 ) ) ?>",
				"radius": "<?php echo esc_attr( get_option( PATTERNLOCK  . "_matrix_radius", 30 ) ) ?>"
			},
			"notifications"	: {
				"forgot"	: "<?php echo esc_attr( get_option( PATTERNLOCK . "_forgot_notification", "If either your e-mail or username are valid, you will a recovery email." ) ) ?>",
				"forgot_error"	: "<?php echo esc_attr( get_option( PATTERNLOCK . "_forgot_notif_t", "Hey, Hey! Slow down buddy!" ) ) ?>",
				"iframe"	: "<?php _e( "For security reasons, framing is not allowed; click OK to remove the frames.", "plock-locale" ) ?>"
			},
			"labels"	: { 
				"second"	: "<?php echo esc_attr( get_option( PATTERNLOCK . "_lock_sec", "second" ) ) ?>",
				"minute" 	: "<?php echo esc_attr( get_option( PATTERNLOCK . "_lock_min", "minute" ) ) ?>",
				"hour"		: "<?php echo esc_attr( get_option( PATTERNLOCK . "_lock_hour", "hour" ) ) ?>",
				"day"		: "<?php echo esc_attr( get_option( PATTERNLOCK . "_lock_day", "day" ) ) ?>",
				"month"		: "<?php echo esc_attr( get_option( PATTERNLOCK . "_lock_month", "month" ) ) ?>",
				"seconds"	: "<?php echo esc_attr( get_option( PATTERNLOCK . "_lock_secs", "second" ) ) ?>",
				"minutes" 	: "<?php echo esc_attr( get_option( PATTERNLOCK . "_lock_mins", "minute" ) ) ?>",
				"hours"		: "<?php echo esc_attr( get_option( PATTERNLOCK . "_lock_hours", "hour" ) ) ?>",
				"days"		: "<?php echo esc_attr( get_option( PATTERNLOCK . "_lock_day", "days" ) ) ?>",
				"months"	: "<?php echo esc_attr( get_option( PATTERNLOCK . "_lock_month", "months" ) ) ?>",
				"forgot_submit"	: "<?php echo esc_attr( get_option( PATTERNLOCK . "_forgot_submit", "Reset Password" ) ) ?>",
				"forgot_cancel"	: "<?php echo esc_attr( get_option( PATTERNLOCK . "_forgot_cancel", "Cancel" ) ) ?>",
				"forgot_placeholder"	: "<?php echo esc_attr( get_option( PATTERNLOCK . "_forgot_field", "Your Username or E-mail Address" ) ) ?>",
				"forgot_title"	: "<?php echo esc_attr( get_option( PATTERNLOCK . "_forgot_title", "Reset Your Password" ) ) ?>",
				"lockdown"	: "<?php echo esc_attr( get_option( PATTERNLOCK . "_lock", "Lockdown" ) ) ?>"
			},
			"ajaxurl"	: "<?php echo admin_url( "admin-ajax.php" ) ?>"
		};/* ]]> */
	</script>
	<script src="<?php echo home_url() . '/wp-includes/js/jquery/jquery.js'?>"></script>
	<script src="<?php echo WP_PLUGIN_URL . '/pattern-lock/lib/patternLock.min.js' ?>"></script>
	<script src="<?php echo WP_PLUGIN_URL . '/pattern-lock/lib/alertify.js' ?>"></script>
	<script src="<?php echo WP_PLUGIN_URL . '/pattern-lock/lib/breakpoints.js' ?>"></script>
	<script src="<?php echo WP_PLUGIN_URL . '/pattern-lock/lib/countdown.min.js' ?>"></script>
	<script src="<?php echo WP_PLUGIN_URL . '/pattern-lock/lib/main-min.js' ?>"></script>
	
</body>
</html>