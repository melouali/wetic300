<?php 

new PatternLockUserProfile();
	
class PatternLockUserProfile{
	
	public function __construct(){
		
		add_action( 'show_user_profile', array( $this, 'display_user_hash' ) );
		add_action( 'edit_user_profile', array( $this, 'display_user_hash' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'load_admin_assets' ) );
		add_action( 'wp_ajax_pattern_lock_set', array( $this, 'ajax_set' ) );
		
	}
	
	
	function load_admin_assets( $hook ){

		if ( 'profile.php' !== $hook && 'user-edit.php' !== $hook ) {
			return;
		}

		wp_enqueue_style( 'pattern_lock', plugin_dir_url( __FILE__ ) . 'lib/patternLock.css', false, null );

		wp_enqueue_script( 'pattern_lock', plugin_dir_url( __FILE__ ) . 'lib/patternLock.min.js', false, null, true );
		wp_enqueue_script( 'pattern_lock_user', plugin_dir_url( __FILE__ ) . 'lib/user-profile.js', array( 'jquery', 'pattern_lock' ), null, true );


		$matrix = array(
			'cols' => esc_attr( get_option( PATTERNLOCK  ."_matrix_columns", 3 ) ),
			'rows' => esc_attr( get_option( PATTERNLOCK  ."_matrix_rows", 3 ) ),
			'margin' => esc_attr( get_option( PATTERNLOCK  ."_matrix_margin", 20 ) ),
			'radius' => esc_attr( get_option( PATTERNLOCK  ."_matrix_radius", 30 ) )
		);

		wp_localize_script( 'pattern_lock_user', 'matrix', $matrix );

	}
	
	
	function ajax_set(){
		
		check_ajax_referer( 'pattern-set', 'nonce' );

		if( ! isset( $_POST['pattern'] ) )
			wp_die();

		$pattern  	= explode( '-', $_POST['pattern'] );
		$min   		= esc_attr( get_option( PATTERNLOCK . "_matrix_required" , 5 ) );
		$cols   	= esc_attr( get_option( PATTERNLOCK  ."_matrix_columns", 4 ) );
		$rows   	= esc_attr( get_option( PATTERNLOCK  ."_matrix_rows", 4 ) );
		$diags   	= $rows >= $cols ? $cols : $row;
		$count   	= count( $pattern );
		$chunks_h  	= array_chunk( $pattern, $rows );
		$chunks_v  	= array_chunk( $pattern, $cols );
		$chunks_d  	= array_chunk( $pattern, $diags );
		$chunks_hr  = array_chunk( array_reverse($pattern), $rows );
		$chunks_vr  = array_chunk( array_reverse($pattern), $cols );
		$chunks_dr  = array_chunk( array_reverse($pattern), $diags );
		$hashes  	= maybe_unserialize( get_option( PATTERNLOCK . '_pattern_lock_hashes' ) );
		$out 		= array();

		if( $count < $min ){

			$out['status'] = false;
			$out['message'] = sprintf( __( 'Your pattern needs a minimum of %d dots!', 'plock-locale' ), $min );

		} 
		
		else if( isset( $_SESSION['pt_hash'] ) && $_SESSION['pt_hash'] !== hash( 'sha256', implode( '-', $pattern ) ) ){

			unset( $_SESSION['pt_hash'] );

			$out['status'] = false;
			$out['message'] = __( 'Your pattern signatures don\'t match. Please try again.', 'plock-locale' );

		}

		else if( $this->check_bar( $pattern, 1 ) ||
				$this->check_bar( array_combine( array_keys( $pattern ), array_reverse( array_values( $pattern ) ) ), 1 ) ||
				$this->check_bar( $pattern, $cols ) ||
				$this->check_bar( array_combine( array_keys( $pattern ), array_reverse( array_values( $pattern ) ) ), $cols ) ||
				$this->check_bar( $pattern, $cols, true ) ||
				$this->check_bar( array_combine( array_keys( $pattern ), array_reverse( array_values( $pattern ) ) ), $cols, true ) ||
				$this->check_bar( $pattern, $cols, false ) ||
				$this->check_bar( array_combine( array_keys( $pattern ), array_reverse( array_values( $pattern ) ) ), $cols, false ) ){

			$out['status'] = false;
			$out['message'] = __( 'Your pattern is too weak!', 'plock-locale' );
		}


		else if( $this->check_chunk( $chunks_h, $cols ) || $this->check_chunk( $chunks_v, $cols ) || $this->check_chunk( $chunks_d, $cols ) ){
			
			$out['status'] = false;
			$out['message'] = __( 'Your pattern is too weak!', 'plock-locale' );
				
		}

		else if( $this->check_chunk( $chunks_hr, $cols ) || $this->check_chunk( $chunks_vr, $cols ) || $this->check_chunk( $chunks_dr, $cols ) ){
			
			$out['status'] = false;
			$out['message'] = __( 'Your pattern is too weak!', 'plock-locale' );
				
		}
		
		else if( hash( 'sha256', implode( '-', $pattern ) ) === get_user_meta( $_POST['id'], '_pattern_lock_hash', true ) ){
			
			$out['status'] = false;
			$out['message'] = __( 'You already have this pattern!', 'plock-locale' );
			
		}

		else if( ! empty( $hashes ) && is_array( $hashes )  ){

			foreach( $hashes as $user => $hash ){
				
				if( hash( 'sha256', implode( '-', $pattern ) ) === $hash ){
					
					$out['status'] = false;
					$out['message'] = __( 'Your pattern is too weak!', 'plock-locale' );
					
				}

			}

		}

		if( empty( $out ) ){

			if( ! isset( $_SESSION['pt_hash'] ) ){

				$out['status'] = 1;
				$_SESSION['pt_hash'] = hash( 'sha256', implode( '-', $pattern ) );

				$out['message'] = __( 'Please enter again your signature to confirm.', 'plock-locale' );

			} else {

				unset( $_SESSION['pt_hash'] );

				$hash = hash( 'sha256', implode( '-', $pattern ) );
				$user_id = $_POST['id'];
				$hash_list = maybe_unserialize( get_option( PATTERNLOCK . '_pattern_lock_hashes' ) );

				$hash_list = ! is_array( $hash_list ) ? array() : $hash_list;
				$hash_list[$user_id] = $hash;

				update_option( PATTERNLOCK . '_pattern_lock_hashes', maybe_serialize( $hash_list ) );

				update_user_meta( $user_id, '_pattern_lock_hash', $hash );

				$out['status'] = true;
				$out['message'] = __( 'Your pattern signature has been successfully saved!', 'plock-locale' );

			}

		}

		wp_send_json( $out );

	}
	
	
	
	function check_chunk( $arrays, $cols ){

		$meter  = count( $arrays[0] );
		$chunks = array();

		foreach( $arrays as $key => $array ){

			if( count( $array ) >= 2 ){

				if( $this->check_bar( $array, 1 ) ||
					$this->check_bar( array_combine( array_keys( $array ), array_reverse( array_values( $array ) ) ), 1 ) ||
					$this->check_bar( $array, $cols ) ||
					$this->check_bar( array_combine( array_keys( $array ), array_reverse( array_values( $array ) ) ), $cols ) ||
					$this->check_bar( $array, $cols, true ) ||
					$this->check_bar( array_combine( array_keys( $array ), array_reverse( array_values( $array ) ) ), $cols, true ) ||
					$this->check_bar( $array, $cols, false ) ||
					$this->check_bar( array_combine( array_keys( $array ), array_reverse( array_values( $array ) ) ), $cols, false )
				){
					$chunks[$key] = false;
				}


			} else {

				$chunks[$key] = false;

			}

		}

		switch ( true ){

		case ( count( $arrays ) <= 2 && count( $chunks ) > 1 ) : {

				return true;

			} break;

		case ( count( $arrays ) <= 3 && count( $chunks ) >= 2 && count( $arrays[1] ) === $meter  && count( $arrays[2] ) === $meter ) : {

				return true;

			} break;

		case ( count( $arrays ) <= 4 && count( $chunks ) >= 3 && count( $arrays[1] ) === $meter  && count( $arrays[2] ) === $meter  && count( $arrays[3] ) === $meter ) : {

				return true;

			} break;

		}

		return false;

	}


	function check_bar( $array, $cols = null, $diagonal = null ){

		$count = count( $array );
		$consec = 1;

		for( $i = 1; $i < $count; $i++ ){

			if( is_null( $diagonal ) ){

				if( intval($array[$i]) === intval($array[$i - 1] + $cols ) ){

					$consec++;

				}

			} else if( $diagonal === true ) {

					if( intval($array[$i]) === intval($array[$i - 1] + $cols + 1 ) ){

						$consec++;

					}

				} else if( $diagonal === false ) {

					if( intval( $array[$i] ) === intval( $array[$i - 1] + $cols - 1 ) ){

						$consec++;

					}

				}

		}

		if( $consec === $count )
			return true;

	}
	
	
	function display_user_hash( $user ) { ?>
		<?php $url = site_url( '/' . esc_attr( get_option( PATTERNLOCK . '_url', 'pattern-lock' ) ) . '/' ); ?>
		
		<h3><?php _e( 'Pattern Lock Settings', 'Pattern Lock' ); ?></h3>
	    <table class="form-table">
	        <tr>
	            <th><label><?php echo  __( 'Unique Pattern Signature', 'plock-locale' ); ?></label></th>
	            <td>
		            <div id="PatternLock" data-nonce="<?php echo wp_create_nonce( 'pattern-set' ) ?>" data-id="<?php echo isset( $_REQUEST['user_id'] ) ? $_REQUEST['user_id'] : get_current_user_id() ?>"></div>
		            <button type="button" class="button button-secondary" id="pt-generate"><?php _e( 'Generate Signature', 'plock-locale' ); ?></button>
		            <p class="description"></p>
	            </td>
	        </tr>
	        <tr>
	            <th><label><?php echo  __( 'Login URL', 'plock-locale' ); ?></label></th>
	            <td>
		            <a href="<?php echo $url ?>"><?php echo $url ?></a><br>
		            <p class="description"><?php _e( "This is your Pattern Lock login URL.", 'plock-locale' ) ?></p>
	            </td>
	        </tr>
	    </table>
	    
	    <?php
	}
	
	
}
	
?>