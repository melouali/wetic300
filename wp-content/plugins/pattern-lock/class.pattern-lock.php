<?php

new PatternLock();

class PatternLock {

	public function __construct(){
		
		add_action( 'plugins_loaded', array( $this, 'text_domain' ) );

		add_filter( 'query_vars', array( $this, 'add_query_vars' ), 0 );
		add_action( 'parse_request', array( $this, 'tag_visitor' ), 0 );
		add_action( 'init', array( $this, 'login_page_rewrite' ) );
		add_action( 'init', array( $this, 'init_session' ) );
		add_action( 'init', array( $this, 'wp_login_redirect' ), 10, 0 );

		add_action( 'wp_ajax_pattern_lock_map', array( $this, 'ajax_map' ) );
		add_action( 'wp_ajax_nopriv_pattern_lock_map', array( $this, 'ajax_map' ) );
		add_action( 'wp_ajax_pattern_lock_login', array( $this, 'ajax' ) );
		add_action( 'wp_ajax_nopriv_pattern_lock_login', array( $this, 'ajax' ) );
		add_action( 'wp_ajax_pattern_lock_forgot', array( $this, 'ajax_forgot' ) );
		add_action( 'wp_ajax_nopriv_pattern_lock_forgot', array( $this, 'ajax_forgot' ) );

		add_filter( 'whitelabel_field_pattern_lock_url', array( $this, 'filter_rewrite' ), 10, 1 );
		add_filter( 'whitelabel_field_pattern_lock_wp_login', array( $this, 'filter_login' ), 10, 1 );
		add_action( 'wp_logout', array( $this, 'go_home' ) );

	}
	
	
	function go_home(){
		
		if( filter_var( esc_attr( get_option( PATTERNLOCK . "_wp_login", 'false' ) ), FILTER_VALIDATE_BOOLEAN ) === true ){
			
			$url = esc_attr( get_option( PATTERNLOCK . "_url", 'pattern-lock' ) );
			$url = site_url( "/$url/" );
	
			wp_safe_redirect( $url );
			
			exit();
			
		}
		
	}
	
	
	function text_domain() {
		load_plugin_textdomain( 'plock-locale', false, dirname( plugin_basename(__FILE__) ) . '/langs/' ); 
	}



	private function flush_security(){

		extract( self::get_user_data() );

		delete_transient( 'pt_atts_' . md5( $user ) );
		delete_transient( 'pt_nonce_' . md5( $user ) );

		if( isset( $_SESSION[ 'pt_login_attempts' ] ) ) unset( $_SESSION[ 'pt_login_attempts' ] );
		if( isset( $_SESSION[ 'pt_nonce' ] ) ) unset( $_SESSION[ 'pt_nonce' ] );

		if( isset( $_COOKIE[ 'pt_login_attempts' ] ) ) unset( $_COOKIE[ 'pt_login_attempts' ] );
		if( isset( $_COOKIE[ 'pt_nonce' ] ) ) unset( $_COOKIE[ 'pt_nonce' ] );

		return;

	}



	private function lockdown_check(){

		$atts = esc_attr( get_option( PATTERNLOCK  ."_att_lockdown", 20 ) );
		$time = esc_attr( get_option( PATTERNLOCK  ."_att_lockdown_time", 30 ) );
		$compromised = explode( ',', trim( str_replace( ' ', '', get_option( PATTERNLOCK . '_compromised' ) ), ',' ) );
		$url = esc_attr( get_option( PATTERNLOCK . "_url", 'pattern-lock' ) );

		if( ! in_array( $url, $compromised ) ){

			if ( is_admin() ) require_once( ABSPATH . 'wp-includes/pluggable.php');

			global $wpdb;

			$log = $wpdb->get_results( "

				SELECT *
				FROM    {$wpdb->prefix}pattern_log
				WHERE   time >=  '" . date( 'Y-m-d H:i:s', strtotime( "-$time minutes" ) ) . "'

			" );

			if( count( $log ) >= $atts ){

				array_push( $compromised, $url );

				update_option( PATTERNLOCK . '_compromised', trim( implode( ',', $compromised ), ',' ) );

				$users = new WP_User_Query( array( 'role' => 'Administrator' ) );
				$email = sprintf( __( "Your Pattern Lock login page has been compromised and locked down. Please login here: %s and change your Pattern Lock Settings.", 'plock-locale' ), site_url( '/' . md5( PATTERNLOCK_KEY ) . '/' ) );

				foreach( $users->results as $user ){

					wp_mail( $user->user_email, __( 'Your Login has been compromised! Please take action now!', 'plock-locale' ), $email );

				}

				self::add_user_to_blacklist( $_SERVER['REMOTE_ADDR'] );

				return true;

			} else {

				return false;

			}

		} else {

			return true;

		}
	}
	
	
	
	private static function get_ip() {
		
		if ( function_exists( 'apache_request_headers' ) ) {
			
			$headers = apache_request_headers();
			
		} else {
			
			$headers = $_SERVER;
		}
		
		if ( array_key_exists( 'X-Forwarded-For', $headers ) && filter_var( $headers['X-Forwarded-For'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 ) ) {
			
			$the_ip = $headers['X-Forwarded-For'];
			
		} elseif ( array_key_exists( 'HTTP_X_FORWARDED_FOR', $headers ) && filter_var( $headers['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 )
		) {
			
			$the_ip = $headers['HTTP_X_FORWARDED_FOR'];
			
		} else {
			
			$the_ip = filter_var( $_SERVER['REMOTE_ADDR'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 );
		}
		
		return $the_ip;
	}



	private static function user_access(){

		if( ! PatternLock::whitelist() ){
			return;
		}

		if( ! PatternLock::blacklist() ){
			return;
		}

		$cookie_nonce 	= isset( $_COOKIE['pt_nonce'] ) ? $_COOKIE['pt_nonce'] : null;
		$cookie_atts  	= isset( $_COOKIE['pt_login_attempts'] ) ? $_COOKIE['pt_login_attempts'] : null;
		$session_nonce 	= isset( $_SESSION['pt_nonce'] ) ? $_SESSION['pt_nonce'] : null;
		$session_atts 	= isset( $_SESSION['pt_login_attempts'] ) ? $_SESSION['pt_login_attempts'] : null;

		$ipaddress 	= self::get_ip();
		$datetime 	= mktime( 0, 0, 0 );

		$user = hash( 'sha256', PATTERNLOCK_KEY . $ipaddress  );

		$transient_nonce	= get_transient( 'pt_nonce_' . md5( $user ) );
		$transient_atts  	= get_transient( 'pt_atts_' . md5( $user ) );

		if( $transient_nonce === false || $transient_atts === false ){
			return;
		}

		if( $transient_nonce !== $user )
			return;
		
		if( filter_var( esc_attr( get_option( PATTERNLOCK . '_force_session') ), FILTER_VALIDATE_BOOLEAN ) === true ){
			
			if ( is_null( $session_nonce ) || is_null( $session_atts ) ){
				return;
			}
			
			if( $session_nonce !== hash( 'sha256', PATTERNLOCK_KEY . $datetime ) )
				return;
			
		}
		
		if( filter_var( esc_attr( get_option( PATTERNLOCK . '_force_cookie') ), FILTER_VALIDATE_BOOLEAN ) === true ){
			
			if ( ( is_null( $cookie_nonce ) || is_null( $cookie_atts ) ) && $transient_atts !== 0 ){
				return;
			}
	
			if( $cookie_nonce !== PATTERNLOCK_KEY && $transient_atts !== 0 )
				return;
			
		}

		return true;

	}



	function wp_login_redirect(){

		if( filter_var( esc_attr( get_option( PATTERNLOCK."_wp_login", 'false' ) ), FILTER_VALIDATE_BOOLEAN ) === true && ! is_user_logged_in() ){

			global $pagenow;

			if( 'wp-login.php' == $pagenow ) {
				
				header('This is not the page you are looking for', true, 404);

				exit();
			}
		}


	}
	
	
	public static function filter_login( $content ){
		
		if( filter_var( $content, FILTER_VALIDATE_BOOLEAN ) === true && strlen( get_user_meta( get_current_user_id(), '_pattern_lock_hash', true ) ) !== 64 ) return __( 'You cannot disable the WP Login without creating a pattern lock signature. ', 'plock-locale' ) . '<a href="' . admin_url( '/profile.php#password' ) . '">' . __( 'Create Signature', 'plock-locale' )  . '</a>';

	}
	


	public static function filter_rewrite( $content ){

		$compromised = get_option( PATTERNLOCK . '_compromised' );

		if( $compromised ){

			$compromised = str_replace( ' ', '', $compromised );
			$compromised = trim( $compromised, ',' );
			$compromised = explode( ',', $compromised );

			if( ! empty( $compromised ) && in_array( $content, $compromised ) ){
				return __( 'This login URL has been compromised. Please use a different one!', 'plock-locale' );
			}
		}

		if( preg_match('/[^a-zA-Z0-9_-]$/s',$content ) ){
			return __( 'Invalid URL format', 'plock-locale' );
		}

	}


	public static function install(){

		global $wpdb;
		$charset_collate = $wpdb->get_charset_collate();
		$table_name = $wpdb->prefix . 'pattern_log';

		$sql = "CREATE TABLE $table_name (
			id mediumint(9) NOT NULL AUTO_INCREMENT,
			time datetime NOT NULL,
			hash char(64) NOT NULL,
			status tinyint(1) NOT NULL,
			data longtext NOT NULL,
			UNIQUE KEY id (id)
		) $charset_collate;";

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta( $sql );

		self::login_page_rewrite();

		global $wp_rewrite;
		$wp_rewrite->flush_rules();

		/**
		 $htaccess = get_home_path() . ".htaccess";

		 $lines = array();

		 insert_with_markers( $htaccess, "PatternLock", $lines) ;
		 */
	}



	function init_session(){
		if( ! session_id() ) session_start();
	}


	private static function get_user_data(){

		$cookie_nonce 	= isset( $_COOKIE['pt_nonce'] ) ? $_COOKIE['pt_nonce'] : null;
		$cookie_atts  	= isset( $_COOKIE['pt_login_attempts'] ) ? $_COOKIE['pt_login_attempts'] : null;
		$session_nonce 	= isset( $_SESSION['pt_nonce'] ) ? $_SESSION['pt_nonce'] : null;
		$session_atts 	= isset( $_SESSION['pt_login_attempts'] ) ? $_SESSION['pt_login_attempts'] : null;

		$hash = hash( 'sha256', PATTERNLOCK_KEY );

		$ipaddress = $_SERVER['REMOTE_ADDR'];
		$datetime 	= mktime( 0, 0, 0 );

		$user = hash( 'sha256', PATTERNLOCK_KEY . $ipaddress  );

		return array(
			'user'  => $user,
			'cookie_nonce'   	=> $cookie_nonce,
			'cookie_atts'   	=> $cookie_atts,
			'session_nonce'  	=> $session_nonce,
			'session_atts'   	=> $session_atts,
			'transient_nonce'  	=> get_transient( 'pt_nonce_' . md5( $user ) ),
			'transient_atts'  	=> get_transient( 'pt_atts_' . md5( $user ) ),
			'ipaddress'   		=> $ipaddress
		);
	}


	private function apply_punishment( $time = null ){

		$user_data = self::get_user_data();
		$user_data['expiry'] = time() + $time;

		extract( $user_data );

		if( $time === true ){

			self::add_user_to_blacklist( $ipaddress );

		} else {

			set_transient( 'pt_punish_' . md5( $user ), $user_data, $time );

		}

	}



	private static function check_punishments(){
		
		$user = hash( 'sha256', PATTERNLOCK_KEY . $_SERVER['REMOTE_ADDR'] );

		$punish  = get_transient( 'pt_punish_' . md5( $user ) );
		
		if( $punish === false ){
			
			return true;

		} else {
			
			extract( self::get_user_data() );
			
			if( $transient_nonce === $punish['transient_nonce'] ){

				return false;

			}
			
			if( filter_var( esc_attr( get_option( PATTERNLOCK . '_force_session') ), FILTER_VALIDATE_BOOLEAN ) === true ){
				
				if( $session_nonce === $punish['session_nonce']){
	
					return false;
	
				}
				
			}
			
			if( filter_var( esc_attr( get_option( PATTERNLOCK . '_force_cookie') ), FILTER_VALIDATE_BOOLEAN ) === true ){
				
				if( $cookie_nonce === $punish['cookie_nonce'] ){

					return false;
	
				}
				
			}

			return true;

		}
	}
	


	function ajax_forgot(){

		check_ajax_referer( 'my-special-forgot-string', 'nonce' );

		if( ! isset( $_POST['input'] ) )
			wp_die();

		$ipaddress = $_SERVER['REMOTE_ADDR'];
		$threshold = esc_attr( get_option( PATTERNLOCK  ."_forgot_threshold", 30 ) );
		$token     = esc_attr( get_option( PATTERNLOCK . "_forgot_token" ) );
		$transient = get_transient( 'pt_forgot_' . md5( $ipaddress ) );

		if( $transient === false ){

			$transient = array( 1, time() );

			set_transient( 'pt_forgot_' . md5( $ipaddress ), $transient, $threshold * MINUTE_IN_SECONDS );


		} else {

			$transient = maybe_unserialize( $transient );
			$transient[0] = $transient[0] + 1;

			set_transient( 'pt_forgot_' . md5( $ipaddress ), $transient, $transient[1] + $threshold * MINUTE_IN_SECONDS - time() );

		}

		if( $transient[0] > 500 )
			wp_die();

		$input = $_POST['input'];

		if( isset( $token ) && ! is_null( $token ) && ! empty( $token ) ){

			if( ! strpos( $input, $token ) ){

				echo 'true';
				wp_die();

			}


			$input = trim( str_replace( $token, '', $input ) );

		}


		$users = get_users( array( 'search' => $input ) );

		foreach( $users as $user ){

			if( $input === $user->user_login || $input === $user->user_email ){

				$transient = get_transient( 'pt_reset_' . md5( $ipaddress ) );
				$hash = hash( 'sha256', $user->user_login );
				$mail = __( "Someone requested that the password be reset for the following account:\n
%s \n
Username: %s \n

If this was a mistake, just ignore this email and nothing will happen. \n

The following link is available for 1 hour and only from the same IP address used when requesting the password reset. To reset your password, visit the following address: \n
%s", 'plock-locale' );

				$mail = sprintf( $mail, site_url('/'), $user->user_login, site_url( "/?pattern-lock-key=$hash" ) );

				if( $transient === false ){

					set_transient( 'pt_reset_' . md5( $ipaddress ), $hash, 1 * HOUR_IN_SECONDS );

				}

				wp_mail( $user->user_email, __( 'Reset Password link for: ', 'plock-locale' ) . site_url('/'), $mail );

				delete_transient( 'pt_forgot_' . md5( $ipaddress ) );

			}

		}

		echo 'true';

		wp_die();
	}


	function ajax_map(){

		if( ! self::user_access() ){
			wp_die();
		}
		
		if( ! self::check_punishments() ){
			
			extract( self::get_user_data() ); 
		
			$punish = get_transient( 'pt_punish_' . md5( $user ) );
			
			if( isset( $punish['transient_nonce'] ) && $transient_nonce === $punish['transient_nonce'] ){

				$expiry = $punish['expiry'];
				
				wp_send_json( array( 'status' => false, 'expiry' => $expiry ) );

			}
			
		} else {
			
			$cols   	= esc_attr( get_option( PATTERNLOCK  ."_matrix_columns", 4 ) );
			$rows   	= esc_attr( get_option( PATTERNLOCK  ."_matrix_rows", 4 ) );
			$points  	= $cols * $rows;
			$out   		= array();
			$ipaddress  = $_SERVER['REMOTE_ADDR'];
			$datetime  	= mktime( date("H"), 0, 0 );
	
			for( $i = 1; $i <= $points; $i++ ){
	
				$out[$i] = md5( PATTERNLOCK_KEY . $ipaddress . $datetime . $i  );
	
			}
	
			if( ! empty( $out ) ){
				wp_send_json( array( 'status' => true, 'map' => $out, 'nonces' => array( 'forgot' => wp_create_nonce( "my-special-forgot-string" ), 'save' => wp_create_nonce( "my-special-string" ) ) ) );
			}
			
		}

		wp_die();

	}


	function ajax(){

		check_ajax_referer( 'my-special-string', 'nonce' );

		if( ! isset( $_POST['pattern'] ) )
			wp_die();

		if( ! self::user_access() ){
			wp_die();
		}

		if( ! self::check_punishments() ){
			wp_die();
		}

		$pattern = $_POST['pattern'];
		$pattern = explode( '-', $pattern );

		$cols   	= esc_attr( get_option( PATTERNLOCK  ."_matrix_columns", 4 ) );
		$rows   	= esc_attr( get_option( PATTERNLOCK  ."_matrix_rows", 4 ) );
		$points  	= $cols * $rows;
		$user_data  = self::get_user_data();
		$datetime  	= mktime( date("H"), 0, 0 );
		$matrix  	= array();

		for( $i = 1; $i <= $points; $i++ ){

			$matrix[$i] = md5( PATTERNLOCK_KEY . $user_data['ipaddress'] . $datetime . $i  );

		}

		foreach( $pattern as $key => $point ){
			$pattern[$key] = array_search( $point, $matrix, true );
		}

		$pattern = implode( '-', $pattern );
		
		$hashes = maybe_unserialize( get_option( PATTERNLOCK . '_pattern_lock_hashes' ) );

		foreach( $hashes as $user => $hash ){

			if( hash( 'sha256', $pattern ) === $hash ){
				$user_id = $user;
				$user = get_user_by( 'id', $user );
				$loginusername = $user->user_login;
			}

		}

		$log = $this->count_attempt();
		$response  = array();

		$tokens = get_transient( 'pt_atts_' . md5( $user_data['user'] ) );
		$tokens = $tokens === false ? 0 : $tokens;

		$stage_1 = esc_attr( get_option( PATTERNLOCK . '_att_1_allowed', 3 ) );
		$stage_2 = esc_attr( get_option( PATTERNLOCK . '_att_2_allowed', 2 ) );
		$stage_3 = esc_attr( get_option( PATTERNLOCK . '_att_3_allowed', 1 ) );

		$stage_1_punishment = esc_attr( get_option( PATTERNLOCK . '_att_1_time', 5 ) );
		$stage_2_punishment = esc_attr( get_option( PATTERNLOCK . '_att_2_time', 10 ) );
		$stage_3_punishment = esc_attr( get_option( PATTERNLOCK . '_att_3_time', 1 ) );
		
		switch( true ){

		case ( $tokens <= $stage_1 ) : {

				$stage = 1;

			} break;

		case ( $tokens <= $stage_1 + $stage_2 ) : {

				$stage = 2;

			} break;

		case ( $tokens <= $stage_1 + $stage_2 + $stage_3 ) : {

				$stage = 3;

			} break;

		}
		
		switch( true ){

			case ( $log > 0 && ! isset( $loginusername ) ) :
	
				$this->log_attempt( false, get_current_user_id() );
				
				$response['status']  = false;
				
				switch( $stage ){
	
					case 1 : $response['message'] = sprintf( esc_attr( get_option( PATTERNLOCK . "_notif_stage_1", 'Invalid Pattern. %d Attempts left!' ) ), $log ); break;
						
					case 2 : $response['message'] = sprintf( esc_attr( get_option( PATTERNLOCK . "_notif_stage_2", 'Invalid Pattern. %d Attempts left!' ) ), $log ); break;
						
					case 3 : $response['message'] = sprintf( esc_attr( get_option( PATTERNLOCK . "_notif_stage_3", 'Invalid Pattern. %d Attempts left!' ) ), $log ); break;
		
				}
				
	
				break;
	
			case ( $log === 0 && ! isset( $loginusername ) ) :
	
				$this->log_attempt( false, get_current_user_id() );
	
				$response['status']  = false;
	
				switch( $stage ){
	
					case 1 :
					
						$response['punishment'] = $stage_1_punishment * MINUTE_IN_SECONDS;
						$response['message']  	= sprintf( esc_attr( get_option( PATTERNLOCK . "_notif_stage_1_lock", 'Invalid Pattern. You have been locked out for %d minutes!' ) ), $stage_1_punishment ); 
						
						break;
						
					case 2 : 
					
						$response['punishment'] = $stage_2_punishment * MINUTE_IN_SECONDS;
						$response['message']  	= sprintf( esc_attr( get_option( PATTERNLOCK . "_notif_stage_2_lock", 'Invalid Pattern. You have been locked out for %d minutes!' ) ), $stage_2_punishment ); 
						
						break;
						
					case 3 : 
					
						$response['punishment'] = $stage_3_punishment * DAY_IN_SECONDS;
						$response['message']  	= sprintf( esc_attr( get_option( PATTERNLOCK . "_notif_stage_3_lock", 'Invalid Pattern. You have been locked out for %d days!' ) ), $stage_3_punishment );
						
						break;
		
				}
				
				$response['lock'] = true;
	
				break;
	
			case ( $log >= 0 && isset( $loginusername ) && is_user_logged_in() ) :
	
				$this->log_attempt( true, get_current_user_id() );
				$this->flush_security();
	
				$response['status']  = true;
				$response['redirect']  = admin_url();
	
				break;
	
			case ( $log >= 0 && isset( $loginusername ) && ! is_user_logged_in() ) :
	
				$this->log_attempt( true, $user_id );
				$this->flush_security();
				
				delete_transient( 'pt_nonce_' . md5( $user_data['user'] ) );
				delete_transient( 'pt_atts_' . md5( $user_data['user'] ) );
	
				wp_set_current_user( $user_id, $loginusername );
				wp_set_auth_cookie( $user_id );
	
				do_action( 'wp_login', $loginusername );
	
				$response['status']  = true;
				$response['redirect']  = admin_url();
	
				break;

		}

		wp_send_json( $response );

	}



	private function count_attempt(){

		extract( self::get_user_data() );
		
		set_transient( 'pt_atts_' . md5( $user ),  intval( get_transient( 'pt_atts_' . md5( $user ) ) ) + 1 );
		
		if( filter_var( esc_attr( get_option( PATTERNLOCK . '_force_session') ), FILTER_VALIDATE_BOOLEAN ) === true ){
			
			$_SESSION['pt_login_attempts']++;
		}
		
		if( filter_var( esc_attr( get_option( PATTERNLOCK . '_force_cookie') ), FILTER_VALIDATE_BOOLEAN ) === true ){
			
			setcookie( "pt_login_attempts", intval( $_COOKIE['pt_login_attempts'] ) + 1, strtotime( '+1 day' ), '/', COOKIE_DOMAIN );
			
		}

		$stage_1 = esc_attr( get_option( PATTERNLOCK . '_att_1_allowed', 3 ) );
		$stage_2 = esc_attr( get_option( PATTERNLOCK . '_att_2_allowed', 2 ) );
		$stage_3 = esc_attr( get_option( PATTERNLOCK . '_att_3_allowed', 1 ) );

		$stage_1_punishment = esc_attr( get_option( PATTERNLOCK . '_att_1_time', 5 ) ) * MINUTE_IN_SECONDS;
		$stage_2_punishment = esc_attr( get_option( PATTERNLOCK . '_att_2_time', 10 ) ) * MINUTE_IN_SECONDS;
		$stage_3_punishment = esc_attr( get_option( PATTERNLOCK . '_att_3_time', 1 ) ) * DAY_IN_SECONDS;

		$tokens = get_transient( 'pt_atts_' . md5( $user ) );
		
		if( $tokens === false ){

			$this->apply_punishment( true );

			return 0;

		}

		if( $tokens <= $stage_1 ){

			if( intval( $tokens ) === intval( $stage_1 ) ){

				$this->apply_punishment( $stage_1_punishment );

			}

			return $stage_1 - $tokens;

		} elseif( $tokens <= $stage_1 + $stage_2 ){

			if( intval( $tokens ) === intval( $stage_1 + $stage_2 ) ){

				$this->apply_punishment( $stage_2_punishment );

			}

			return $stage_1 + $stage_2 - $tokens;

		} elseif( $tokens <= $stage_1 + $stage_2 + $stage_3 ){

			if( intval( $tokens ) === intval( $stage_1 + $stage_2 + $stage_3 ) ){

				$this->apply_punishment( $stage_3_punishment );

			}

			return $stage_1 + $stage_2 + $stage_3 - $tokens;

		} else {

			$this->apply_punishment( true );

			return 0;

		}

	}

	private function log_attempt( $status, $user_id = 0 ){

		extract( self::get_user_data() );

		$transient = get_transient( 'pt_nonce_' . md5( $user ) );

		global $wpdb;

		$wpdb->insert(
			$wpdb->prefix . 'pattern_log',
			array(
				'time' => current_time( 'mysql' ),
				'hash' => $transient,
				'status' => $status === true ? 1 : 0,
				'data' => maybe_serialize( array( $_SERVER['REMOTE_ADDR'], $_SERVER['HTTP_REFERER'], $_SERVER['HTTP_USER_AGENT'], $user_id ) ),
			)
		);

	}



	public function add_query_vars( $vars ){
		$vars[] = '__api';
		return $vars;
	}

	public static function login_page_rewrite(){

		$link = md5( PATTERNLOCK_KEY );
		$rewrite = esc_attr( get_option( PATTERNLOCK . "_url", 'pattern-lock' ) );

		add_rewrite_rule( "^$rewrite/?",'index.php?__api=1','top' );
		add_rewrite_rule( "^$link/?",'index.php?__api=2','top' );

	}

	public function tag_visitor(){
			
		global $wp;

		if( isset( $wp->query_vars['__api'] ) ){
			
			if( is_user_logged_in() ){
			
				wp_safe_redirect( admin_url( '/' ) );
				
				$this->flush_security();
				
				delete_transient( 'pt_nonce_' . md5( $user_data['user'] ) );
				delete_transient( 'pt_atts_' . md5( $user_data['user'] ) );
				
				exit();
			}

			$lockdown = $this->lockdown_check();

			if( intval( $wp->query_vars['__api'] ) === 1 && $lockdown === false ){

				PatternLock::apply_tag();

			} elseif( intval( $wp->query_vars['__api'] ) === 2 && $lockdown === true ){
				
				PatternLock::apply_tag();

			} else {

				header( 'HTTP/1.0 404 Not Found' );

			}

			exit;

		} else if( isset( $_GET['pattern-lock-key'] ) ){

				$ipaddress = $_SERVER['REMOTE_ADDR'];
				$transient = get_transient( 'pt_reset_' . md5( $ipaddress ) );

				if( $transient ){

					$users = get_users();

					foreach( $users as $user ){

						if(  $_GET['pattern-lock-key'] === hash( 'sha256', $user->user_login ) ){

							wp_set_current_user( $user->ID, $user->user_login );
							wp_set_auth_cookie( $user->ID );

							do_action( 'wp_login', $user->user_login );

							delete_transient( 'pt_reset_' . md5( $ipaddress ) );

							wp_safe_redirect( admin_url( '/profile.php#password' ) );
						}

					}

				}


				exit;

			}
	}


	private static function apply_tag(){

		$ipaddress  = self::get_ip();
		$datetime  	= mktime( 0, 0, 0 );

		$user = hash( 'sha256', PATTERNLOCK_KEY . $ipaddress );

		$transient_nonce  	= get_transient( 'pt_nonce_' . md5( $user ) );
		$transient_atts  	= get_transient( 'pt_atts_' . md5( $user ) );

		if( $transient_nonce === false )
			set_transient( 'pt_nonce_' . md5( $user ), $user, 24 * HOUR_IN_SECONDS );

		if( $transient_atts === false )
			set_transient( 'pt_atts_' . md5( $user ), 0, 24 * HOUR_IN_SECONDS );
		
		if( filter_var( esc_attr( get_option( PATTERNLOCK . '_force_session') ), FILTER_VALIDATE_BOOLEAN ) === true ){
			
			if( ! isset( $_SESSION[ 'pt_nonce' ] ) )
				$_SESSION[ 'pt_nonce' ] = hash( 'sha256', PATTERNLOCK_KEY . $datetime );

			if( ! isset( $_SESSION[ 'pt_login_attempts' ] ) )
				$_SESSION[ 'pt_login_attempts' ] = 0;
	
			if( $transient_atts !== false && isset( $_SESSION[ 'pt_login_attempts' ] ) && $_SESSION[ 'pt_login_attempts' ] !== $transient_atts )
				$_SESSION[ 'pt_login_attempts' ] = $transient_atts;
			
		}
		
		if( filter_var( esc_attr( get_option( PATTERNLOCK . '_force_cookie') ), FILTER_VALIDATE_BOOLEAN ) === true ){
			
			if ( ! isset( $_COOKIE[ "pt_nonce" ] ) )
				setcookie( "pt_nonce", PATTERNLOCK_KEY, strtotime( '+1 day' ), '/', COOKIE_DOMAIN );
	
			if ( ! isset( $_COOKIE[ "pt_login_attempts" ] ) )
				setcookie( "pt_login_attempts", 0, strtotime( '+1 day' ), '/', COOKIE_DOMAIN );
	
			if( $transient_atts !== false && isset( $_COOKIE[ "pt_login_attempts" ] ) && $_COOKIE[ "pt_login_attempts" ] !== $transient_atts )
				$_COOKIE[ "pt_login_attempts" ] = $transient_atts;
			
		}

		include( 'login-page.php' );

	}



	public static function whitelist(){

		$whitelist = esc_attr( get_option( PATTERNLOCK . '_whitelist', '' ) );

		if( isset( $whitelist ) && ! is_null( $whitelist ) && ! empty( $whitelist ) ){

			$whitelist = str_replace( ' ', '', $whitelist );
			$whitelist = trim( $whitelist, ',' );
			$whitelist = explode( ',', $whitelist );

			if( in_array( $_SERVER['REMOTE_ADDR'], $whitelist ) )
				return true;
			else
				return false;

		} else {

			return true;

		}
	}

	public static function blacklist(){

		$blacklist = esc_attr( get_option( PATTERNLOCK . '_blacklist', '' ) );
		$blacklist = str_replace( ' ', '', $blacklist );
		$blacklist = trim( $blacklist, ',' );
		$blacklist = explode( ',', $blacklist );

		if( in_array( $_SERVER['REMOTE_ADDR'], $blacklist ) )
			return false;
		else
			return true;

	}

	private static function add_user_to_blacklist( $ip ){

		$blacklist = esc_attr( get_option( PATTERNLOCK . '_blacklist', '' ) );
		$blacklist = str_replace( ' ', '', $blacklist );
		$blacklist = trim( $blacklist, ',' );
		$blacklist = explode( ',', $blacklist );
		$blacklist[] = filter_var( $ip, FILTER_VALIDATE_IP );
		$blacklist = array_unique( $blacklist );
		$blacklist = implode(',', $blacklist );
		$blacklist = trim( $blacklist, ',' );

		update_option( PATTERNLOCK . '_blacklist',  $blacklist );
	}

	public static function hex2rgb( $colour, $array = false ){

		if ( $colour[0] == '#' ) {
			$colour = substr( $colour, 1 );
		}
		if ( strlen( $colour ) == 6 ) {
			list( $r, $g, $b ) = array( $colour[0] . $colour[1], $colour[2] . $colour[3], $colour[4] . $colour[5] );
		} elseif ( strlen( $colour ) == 3 ) {
			list( $r, $g, $b ) = array( $colour[0] . $colour[0], $colour[1] . $colour[1], $colour[2] . $colour[2] );
		} else {
			return false;
		}

		$r = hexdec( $r );
		$g = hexdec( $g );
		$b = hexdec( $b );
		
		if( $array )
			return array( 'r' => $r, 'g' => $g, 'b' => $b );
		
		return "$r, $g, $b";
	}

	public static function brightness( $colour, $opacity ){

		extract( self::hex2rgb( $colour, true) );

		if( $r + $g + $b > 382 ){

			return "rgba(0, 0, 0, $opacity)";

		} else {

			return "rgba(255, 255, 255, $opacity)";
		}
	}

}


?>