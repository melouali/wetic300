(function($) {
	"use strict";
	
	var lock = new PatternLock( "#PatternLock", { 
			matrix:[matrix.cols, matrix.rows],
			margin: parseInt( matrix.margin ),
			radius: parseInt( matrix.radius ),
			delimiter: '-',
			onDraw:function(pattern){
				
				lock.disable();
				
				var data = {
					'action': 'pattern_lock_set',
					'pattern': pattern,
					'nonce' : $("#PatternLock").data('nonce'),
					'id'	: $("#PatternLock").data('id')
				};
		        $.post( ajaxurl, data, function(response) { 
			      
					if( response.status === true || response.status === 1 ){
						
						$(".patt-holder").addClass("patt-success");
						$('#PatternLock').parents('td').removeClass('error').addClass('success');
						
						setTimeout( function(){
							
							lock.reset();
							lock.enable();
							
							$(".patt-holder").removeClass("patt-success");
							$('#PatternLock').parents('td').removeClass('success');
							
							if( response.status === true ){
								$("#PatternLock").hide();
								$('#pt-generate').show();
							}
							
						}, 2000 );
						
					} else if( response.status === false ){
						
						lock.error();
						
						$('#PatternLock').parents('td').addClass('error');
						
						setTimeout( function(){
							lock.reset();
							lock.enable();
							$('#PatternLock').parents('td').removeClass('error');
						}, 1000 );
						
					}
					
					$('#PatternLock').siblings('.description').text( response.message );
					
				});
				
		    }
		});
		
	$('#PatternLock').toggle();
	
	$('#pt-generate').click(function(){
		
		$(this).hide();
		
		$('#PatternLock').toggle();
		
	});
	
})(jQuery);