(function($) {
	"use strict";
	
	var $nonces;
	
	if( top !== self ){
	    top.location.replace( document.location );
	    alert( PatternLockData.notifications.iframe );
	}
	
	countdown.setLabels(
				' | ' + PatternLockData.labels.second + '| ' + PatternLockData.labels.minute + '| ' + PatternLockData.labels.hour + '| ' + PatternLockData.labels.day + '| | ' + PatternLockData.labels.month + '|',
				' | ' + PatternLockData.labels.seconds + '| ' + PatternLockData.labels.minutes + '| ' + PatternLockData.labels.hours + '| ' + PatternLockData.labels.days + '| | ' + PatternLockData.labels.months + '|',
				', ',
				', '
	);
	
	function call_forgot_password(){
		
		alertify
			.logPosition("bottom left")
			.okBtn( PatternLockData.labels.forgot_submit )
			.cancelBtn( PatternLockData.labels.forgot_cancel )
			.placeholder( PatternLockData.labels.forgot_placeholder )
			.prompt( PatternLockData.labels.forgot_title,
				function (val, ev) {
					
					ev.preventDefault();
					
					var data = {
						'action': 'pattern_lock_forgot',
						'nonce'	: $nonces.forgot,
						'input' : val
					};
					
					$.post( PatternLockData.ajaxurl, data, function(response) {
						
						if( Boolean( response ) === true ){
							
							alertify.success( PatternLockData.notifications.forgot );
							
						} else {
							
							alertify.error( PatternLockData.notifications.forgot_error );
							
						}
						
					});

					
					
					
				}, function(ev) {

					ev.preventDefault();
					$(document).keyup(keyUpFunc);
					
				}
		);
		
	}
	
	function keyUpFunc(e) {
		if ( e.keyCode === 27 ) { call_forgot_password(); $(document).unbind( "keyup", keyUpFunc ); }
	}
	
	$('#escape > kbd').click(function(){
		
		call_forgot_password();
		
	});
	
	$(document).keyup(keyUpFunc);
	
	var lock;
	
	function send_pattern( pattern ){
		
		lock.disable();
							
		var data = {
			'action': 'pattern_lock_login',
			'nonce': $nonces.save,
			'pattern': pattern
		};
		
        $.post( PatternLockData.ajaxurl, data, function(response) { 

			if( Boolean( response.status ) === true && response.redirect.length >= 5 ){
				
				$(".patt-holder").addClass("patt-success");
				
				setTimeout( function(){
					
					window.location.replace(response.redirect);
					
				}, 1000 );
				
			} else if( Boolean( response.status ) === false ){
				
				alertify.logPosition("top right").delay(2000).error( response.message );
				
				lock.error();
				
				if( response.lock !== 'undefined' && Boolean( response.lock  ) === true ){
					
					call_lockdown( parseInt( response.punishment ) );
					
				} else {
					
					setTimeout( function(){
							
						call_matrix();
						
					}, 1000 );
					
				}
				
				
			} else if( parseInt( response.status ) === 0 ){
				
				call_lockdown( false );
				
			}
			
		});
		
	}
	
	
	
	function breakpoint( $options, $show ){
		
		lock.option( 'radius', parseInt( $options[0] ) );
		lock.option( 'margin', parseInt( $options[1] ) );
		
		if( $show === true ){
			$('#escape').show();
		} else {
			$('#escape').hide();
		}
		
		
	}
	
	
	
	function define_breakpoints(){
		
		var points = parseInt( PatternLockData.matrix.cols ) * parseInt( PatternLockData.matrix.rows );
		
		var $320_radius = 15;
		var $320_margin = 15;
		var $480_radius = 15;
		var $480_margin = 15;	
			
		switch( true ){
			
			case points <= 9 : {
				
				$320_radius = 25;
				$320_margin = 20;
				$480_radius = 30;
				$480_margin = 20;
				
			} break;
			
			case points <= 16 : {
				
				$320_radius = 25;
				$320_margin = 10;
				$480_radius = 25;
				$480_margin = 20;
				
			} break;
			
			case points <= 25 : {
				
				$320_radius = 20;
				$320_margin = 10;
				$480_radius = 20;
				$480_margin = 15;
				
			} break;
			
			case points <= 49 : {
				
				$320_radius = 15;
				$320_margin = 8;
				$480_radius = 20;
				$480_margin = 10;
				
			} break;
			
			case points <= 100 : {
				
				$320_radius = 10;
				$320_margin = 7;
				$480_radius = 12;
				$480_margin = 10;
				
			} break;
			
		}
		
		$(window).bind('enterBreakpoint320',function() {
			breakpoint( new Array( $320_radius, $320_margin ), true );
		});
		
		$(window).bind('enterBreakpoint480',function() {
			breakpoint( new Array( $480_radius, $480_margin ), true );
		});
		
		$(window).bind('enterBreakpoint768',function() {
			breakpoint( new Array( PatternLockData.matrix.radius, PatternLockData.matrix.margin ), false );
		});
		
		$(window).bind('enterBreakpoint1024',function() {
			breakpoint( new Array( PatternLockData.matrix.radius, PatternLockData.matrix.margin ), false );
		});
		
	}
	
	
	
	function call_lockdown( duration ){
		
		$('.spinner').hide();
		$('#PatternLock').hide();
		$('#counter').show();
		
		if( Boolean( duration ) === false ){
			
			$('#counter').html( '<h1>' + PatternLockData.labels.lockdown + '</h1>' );
			
			
		} else {
			
			var timerId = countdown(
			    new Date( parseInt( duration ) * 1000 ),
			    function(ts) {
					$('#counter').html( '<h1>' + PatternLockData.labels.lockdown + '</h1>' + ts.toHTML('span') );
			    },
			    'countdown.DAYS|countdown.HOURS|countdown.MINUTES|countdown.SECONDS'
			);
			
			var expiry 	= new Date( parseInt( duration ) * 1000 ).getTime();
			var now 	= $.now();
			
			setTimeout(function(){
			
				$('#counter').hide();
				$('.spinner').show();
				
				window.clearInterval(timerId);
				
				call_matrix();
				
			}, expiry - now + 500 );
			
		}
		
	}
	
	
	
	function generate_matrix(response){
		
		$("#PatternLock").show();
		$('.spinner').hide();
	
		lock = new PatternLock( "#PatternLock", { 
			matrix:[ parseInt( PatternLockData.matrix.cols ), parseInt( PatternLockData.matrix.rows )],
			margin: parseInt( PatternLockData.matrix.margin ),
			radius: parseInt( PatternLockData.matrix.radius ),
			delimiter: '-',
			mapper: response.map,
			onDraw:function(pattern){ 
				send_pattern( pattern );
		    }
		});
		
		lock.enable();
		
		define_breakpoints();
	
		$(window).setBreakpoints();
		
	}
	
	function call_matrix(){
		
		var data = {
			'action': 'pattern_lock_map',
		};
		
		$.post( PatternLockData.ajaxurl, data, function(response) { 
				console.log(response);
				if( Boolean( response.status ) === true ){
					
					$nonces = response.nonces;
					
					generate_matrix( response );
					
				} else {
					
					call_lockdown( parseInt( response.expiry ) );
					
				}
				
		});
		
	}
	
	call_matrix();
	
	
	
})(jQuery);